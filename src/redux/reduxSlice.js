import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { giaoTiepAPI } from "./giaoTiepAPI";
import { myLocalStore } from "./myLocalStore";
let isLogin = false;
const user = myLocalStore.goiLocalStore("user");
if (user !== null) {
  isLogin = true;
}
const initialState = {
  isLogin,
  user,
  danhSachNguoiDung: [],
  danhSachPhim: [],
  thongTinPhimCanSua: {},
};
const reduxSlice = createSlice({
  name: "nguoiDung",
  initialState,
  reducers: {
    dangNhap: (state, action) => {
      state.isLogin = true;
      state.user = action.payload;
    },
    dangXuat: (state, action) => {
      state.isLogin = false;
      state.user = null;
    },
    chinhSuaPhim: (state, action) => {
      state.thongTinPhimCanSua = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getAllUser.fulfilled, (state, action) => {
      state.danhSachNguoiDung = action.payload;
    });
    builder.addCase(getAllDanhSachPhim.fulfilled, (state, action) => {
      state.danhSachPhim = action.payload;
    });
  },
});
export const getAllUser = createAsyncThunk("nguoiDung/getAllUser", async () => {
  const result = await giaoTiepAPI.layThongTinNguoiDung();
  return result.data.content;
});
export const getAllDanhSachPhim = createAsyncThunk(
  "nguoiDung/getAllDanhSachPhim",
  async () => {
    const result = await giaoTiepAPI.layDanhSachPhim();
    return result.data.content;
  }
);
export const { dangNhap, dangXuat } = reduxSlice.actions;
export default reduxSlice.reducer;

import "./film.scss";
import React, { useEffect, useState } from "react";
import { giaoTiepAPI } from "../../redux/giaoTiepAPI";
import { useDispatch, useSelector } from "react-redux";
import { getAllDanhSachPhim, getAllUser } from "../../redux/reduxSlice";
import { useFormik } from "formik";
import * as yup from "yup";
import { Button, Popconfirm, message } from "antd";
import moment from "moment";
import FormUpload from "../antd/FormUpload";
import UpHinh from "../antd/UpHinh";

const Film = () => {
  const dispatch = useDispatch();
  const [myWidth, setMyWidth] = useState(0);
  const [myEdit, setMyEdit] = useState(false);
  useEffect(() => {
    dispatch(getAllDanhSachPhim());
  }, [dispatch]);
  const { danhSachPhim } = useSelector((state) => state.duLieu);
  const handleXoa = (maPhim) => {
    giaoTiepAPI
      .xoaPhim(maPhim)
      .then((result) => {
        dispatch(getAllDanhSachPhim());
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const handleSua = (phimCanSua) => {
    setMyWidth(400);
    setMyEdit(true);
  };
  const [messageApi, contextHolder] = message.useMessage();
  const formik = useFormik({
    initialValues: {
      tenPhim: "",
      trailer: "",
      moTa: "",
      soSao: "",
      ngayKhoiChieu: "",
      dangChieu: "",
      sapChieu: "",
      hot: "",
      hinhAnh: "",
    },
    validationSchema: yup.object({}),
    onSubmit: (values) => {
      if (myEdit) {
        giaoTiepAPI
          .capNhatThongTinNguoiDung(values)
          .then((result) => {
            messageApi.success("Cập nhật phim thành công");
            dispatch(getAllUser());
            setMyWidth(0);
            setMyEdit(false);
          })
          .catch((error) => {
            messageApi.error("Cập nhật phim thất bại");
          });
      } else {
        giaoTiepAPI
          .themNguoiDung(values)
          .then((result) => {
            messageApi.success("Thêm phimg thành công");
            dispatch(getAllUser());
            setMyWidth(0);
            setMyEdit(false);
          })
          .catch((error) => {
            messageApi.error("Thêm phim thất bại");
          });
      }
    },
  });
  const { handleBlur, handleChange, handleSubmit, values } = formik;
  const handleTimKiemPhim = (event) => {
    const tenPhim = event.target.value.trim().toLowerCase();
    const ketQuaTimKiem = danhSachPhim.filter((phim) =>
      phim.tenPhim.toLowerCase().includes(tenPhim)
    );
    setDanhSachTimKiem(ketQuaTimKiem);
  };
  const [danhSachTimKiem, setDanhSachTimKiem] = useState(danhSachPhim);
  useEffect(() => {
    console.log("danhSachTimKiem:", danhSachTimKiem);
    setDanhSachTimKiem(danhSachPhim);
  }, [danhSachPhim]);

  return (
    <div id="film">
      {contextHolder}
      <button
        type="button"
        id="addFilm"
        onClick={() => {
          if (myWidth === 0) {
            setMyWidth(400);
            setMyEdit(false);
            formik.resetForm();
          } else {
            setMyWidth(0);
            setMyEdit(false);
            formik.resetForm();
          }
        }}
      >
        Thêm phim
      </button>
      <div className="timKiem">
        <input
          id="tenPhim"
          type="text"
          placeholder="Tìm tên phim..."
          onKeyUp={handleTimKiemPhim}
          autoFocus
        />
      </div>
      <div className="myTable">
        <table>
          <thead>
            <tr>
              <th>Mã Phim</th>
              <th>Hình</th>
              <th>Tên Phim</th>
              <th>Ngày Khởi Chiếu</th>
              <th>Thao Tác</th>
            </tr>
          </thead>
          {/* <tbody>
            {danhSachTimKiem.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{item.maPhim}</td>
                  <td>
                    <img src={item.hinhAnh} alt="" />
                  </td>
                  <td
                    style={{
                      fontSize: "16px",
                      fontWeight: "500",
                    }}
                  >
                    {item.tenPhim}
                  </td>
                  <td>
                    <span style={{ color: "green", width: "100px" }}>
                      {moment(item.ngayKhoiChieu).format("DD/MM/YYYY")}
                    </span>
                    <span style={{ color: "red" }}>
                      {moment(item.ngayKhoiChieu).format(" ~ hh:mm")}
                    </span>{" "}
                  </td>
                  <td>
                    <button type="button" onClick={() => handleSua(item)}>
                      Sửa
                    </button>
                    <Popconfirm
                      placement="top"
                      title="Xoá phim"
                      description="Bạn chắc muốn xoá không?"
                      onConfirm={() => handleXoa(item.maPhim)}
                      okText="Xoá"
                      cancelText="Không"
                    >
                      <Button id="myButtonDangXuat">
                        <span>Xoá</span>
                      </Button>
                    </Popconfirm>
                  </td>
                </tr>
              );
            })}
          </tbody> */}
          <tbody>
            {Array.isArray(danhSachTimKiem) && danhSachTimKiem.length > 0 ? (
              danhSachTimKiem.map((item, index) => {
                return (
                  <tr key={index}>
                    <td>{item.maPhim}</td>
                    <td>
                      <img src={item.hinhAnh} alt="" />
                    </td>
                    <td
                      style={{
                        fontSize: "16px",
                        fontWeight: "500",
                      }}
                    >
                      {item.tenPhim}
                    </td>
                    <td>
                      <span style={{ color: "green", width: "100px" }}>
                        {moment(item.ngayKhoiChieu).format("DD/MM/YYYY")}
                      </span>
                      <span style={{ color: "red" }}>
                        {moment(item.ngayKhoiChieu).format(" ~ hh:mm")}
                      </span>{" "}
                    </td>
                    <td>
                      <button type="button" onClick={() => handleSua(item)}>
                        Sửa
                      </button>
                      <Popconfirm
                        placement="top"
                        title="Xoá phim"
                        description="Bạn chắc muốn xoá không?"
                        onConfirm={() => handleXoa(item.maPhim)}
                        okText="Xoá"
                        cancelText="Không"
                      >
                        <Button id="myButtonDangXuat">
                          <span>Xoá</span>
                        </Button>
                      </Popconfirm>
                    </td>
                  </tr>
                );
              })
            ) : (
              <tr>
                <td colSpan="5">Không có kết quả tìm kiếm</td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
      <div id="myForm" style={{ width: myWidth }}>
        <h1>Quản lý phim</h1>
        <UpHinh myEdit={myEdit} />
        <button
          className="myClose"
          type="button"
          onClick={() => {
            setMyWidth(0);
            setMyEdit(false);
            formik.resetForm();
          }}
        >
          Close
        </button>
      </div>
    </div>
  );
};
export default Film;
